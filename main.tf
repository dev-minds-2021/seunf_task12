# Configure the AWS Provider

provider "aws" {
  region = "eu-west-1"
  profile = var.aws_profile
}

#Create a VpC

resource "aws_vpc" "MyVPC" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "MyVPC"
  }
}

#Create IGW

resource "aws_internet_gateway" "MyIGW" {
  vpc_id = aws_vpc.MyVPC.id

  tags = {
    Name = "MyIGW"
  }
}

#Create route Table 
resource "aws_route_table" "MyRT" {
  vpc_id = aws_vpc.MyVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.MyIGW.id
  }

  tags = {
    Name = "main"
  }
}

#Create Subnet
resource "aws_subnet" "subnet1" {
  vpc_id     = aws_vpc.MyVPC.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-west-1a"

  tags = {
    Name = "subnet1"
  }
}

# Create Route table association
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.MyRT.id
}

# SG association
resource "aws_security_group" "web_traffic" {
  name        = "allow_traffic"
  description = "Allow inbound traffic"
  vpc_id      = aws_vpc.MyVPC.id

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_traffic"
  }
}
# Create Instance 

resource "aws_instance" "test_server" {
  ami = "ami-096f43ef67d75e998"
  instance_type = "t2.micro" 
    count = 2
  key_name = "my_keys"
  tags = {
    Name = "webserver"
  }

}

resource "aws_key_pair" "my_keys" {
  key_name   = "my_keys"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDYs5I4Y9Ru914hdeiBWGejAoaIyreCVzvPGXR9shXBp81ZyDIgFYxDmoUIDbyRVY0S4gR0mlqgpFHBiRCBP0lqy3kJ1XRRPSy+GN4ksCxrMpBcW/NqA1osxzVAbE7/0sKNkBO2xVzr3gM4XRXouWu17oahaZHU5rcclKSnjuwJ/48Xws1leQ/Yj2ilDFiw9UG7/lWqxmIVtwXNdUvZRxn7hdFpAAHW32HCW1uBS5djfJxSgGqXAx14K5/8cFGYTrZYcfWrJWHExTM7+SWSVIN3J9OhIxYa7eI3DslNW2cSMj4SA5Ezvq72gqO3hDBMYKpL6uSPJ7rhilp8JxCWf2p6mCmi1c57dkwoK3UEGExdfFetBDI57EiX1dq9l/S6Hrs2vXIkyJluUlHGHblktHkz4ga0hcJXay2gRcZCFIXF0ZpBjOo5FVGPmC8As1FVMdoCMTwhkDhd2fqD4ygo/Yu3iO14Cxka7EnmSb84s2HAlYcFZykgrFj34VWTX3FMt7OWHseA0irGeIa1fSCoWaGR33aZdMNhqTagDh9xd61icl/mwiNP5sKbxz8ZplN1/Z/mEQcjcBIn7UGdKZINuL3s49qM7ZdAYN5CPu0Xbne88Crq3eKoeIaGwbMTCioCc+75LTRbNUITlXPYb+jBf963xpuCbAzZDCDPRJflNqBXzw== abbey@SP.local"
}

